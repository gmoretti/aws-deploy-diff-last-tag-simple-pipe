FROM atlassian/pipelines-awscli:1.16.185

RUN apk update && apk add ca-certificates && rm -rf /var/cache/apk/*

COPY pipe.sh /

ENTRYPOINT ["/pipe.sh"]
